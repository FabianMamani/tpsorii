# Trabajo Practico Inicial de Sistemas Operativos y Redes Inicial

## Device Drivers - Char Device


- **MODULO HOLA MUNDO**

    **Precondicion:**
       
        - En primer lugar se verifico la versión del Kernel con el comando uname -r.

        - Una vez obtenido la version del kernel procedemos a la instalación de los paquetes que necesitamos para trabajar 
         (module-init-header, linux-headers-$(uname -r)).
 


    **Desarrollo del Ejercicio "Hola Mundo"**

    Se clono desde el repositorio que los docentes proveeron para poder afrontar este ejercicio.

    Dicho proyecto contenia un archivo Makefile que compilaba el archivo miModulo.c

    Una vez clonado el repositorio se ingreso a la terminal y se ingresa el siguiente comando:

        sudo E- make

    Dicho comando compilara el archivo Makefile creando el archivo miModulo.o y miModulo.ko.



- **MODULO CHAR DEVICE**


**1.Definir funciones init_module y cleanup_module** 

    La función init_module se utiliza para la instalación de un módulo, en cambio la funcion cleanup_module se encargar de la desinstalación del mismo. Ambas funciones son llamadas por el Kernel.
  
**2.Definir funciones device_open y device_release**

    La funcion device_open se encarga de abrir el device implementado, por otra parte la funcion device_release se encarga de cerrar el device.

